package com.helloword.FirstMicroservice.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.helloword.FirstMicroservice.models.Greeting;
import com.helloword.FirstMicroservice.models.Person;

@RestController
public class MainController {
	
	@RequestMapping("/greeting")
	public Greeting greeting() {
		return new Greeting("Hello World!");
	}
	
	@RequestMapping("/persons")
	public List<Person> allPersons() {
		List<Person> listPersons = new ArrayList<>();
		listPersons.add(new Person("Lucia", "Sanchez", 19));
		listPersons.add(new Person("Juan Felipe", "Cardona", 22));
		listPersons.add(new Person("Andrés", "Velez", 26));
		listPersons.add(new Person("Juan David", "Cano", 18));
	    
	    return listPersons;
	}
}
