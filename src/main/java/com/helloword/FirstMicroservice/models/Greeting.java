package com.helloword.FirstMicroservice.models;

public class Greeting {
	private final String CONTENT;
	
	public Greeting(String CONTENT) {
		super();
		this.CONTENT = CONTENT;
	}
	
	public String getCONTENT() {
		return CONTENT;
	}
}
