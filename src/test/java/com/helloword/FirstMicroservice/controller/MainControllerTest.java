package com.helloword.FirstMicroservice.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.helloword.FirstMicroservice.models.Greeting;
import com.helloword.FirstMicroservice.models.Person;

@RunWith(MockitoJUnitRunner.class)
public class MainControllerTest {
	@InjectMocks MainController mainController;
	@Mock MainController mainControllerMock;
	@Mock Greeting greeting;
	@Mock Person person;
	
	@Test
	public void testGreeting() {
		greeting = new Greeting("Hello World!");
		assertEquals(greeting.getCONTENT(), mainController.greeting().getCONTENT());
	}
	
	@Test
	public void testAllPersonsMock() {
		List<Person> listPersons = new ArrayList<Person>();
		person = new Person("Yessica", "Perez", 23);
		listPersons.add(person);
		
		when(mainControllerMock.allPersons()).thenReturn(listPersons);
		assertTrue(mainController.allPersons() instanceof ArrayList);
		assertNotNull(mainControllerMock.allPersons());
	}

}
